
# PGMrepository

A repository for as many PGMs files and formats as possible ,with some benchmarks.

# Content

Benchmarks for (almost) all the models in this repository using aGrUM's non-scheduled Lazy Propagation (tag 0.22.7).
Time average on 20 runs.

## bif

|    file                        |      structure    | |   load    |  inference |
|:------------------------------:|----------:|--------:|----------:|-----------:|
| `Barley.bif`                   |   48 nodes|  84 arcs|   397.6 ms| 1,127.9 ms |
| `Diabetes.bif`                 |  413 nodes| 602 arcs|   989.2 ms|   226.8 ms |
| `Link.bif`                     |  724 nodes|1125 arcs|   178.9 ms|86,381.8 ms |
| `Mildew.bif`                   |   35 nodes|  46 arcs|   778.2 ms|   204.8 ms |
| `Munin1.bif`                   |  186 nodes| 273 arcs|    74.0 ms|17,906.0 ms |
| `Pigs.bif`                     |  441 nodes| 592 arcs|    70.9 ms|   116.8 ms |
| `Water.bif`                    |   32 nodes|  66 arcs|    54.4 ms|   247.5 ms |
| `alarm.bif`                    |   37 nodes|  46 arcs|     3.5 ms|     4.5 ms |
| `asia.bif`                     |    8 nodes|   8 arcs|     0.3 ms|     0.7 ms |
| `carpo.bif`                    |   61 nodes|  74 arcs|    11.3 ms|     6.9 ms |
| `child.bif`                    |   20 nodes|  25 arcs|     1.6 ms|     2.5 ms |
| `complete_carpo.bif`           |   61 nodes|  74 arcs|     3.4 ms|     7.1 ms |
| `covid19.bif`                  |   39 nodes|  46 arcs|     3.8 ms|     4.4 ms |
| `hailfinder.bif`               |   56 nodes|  66 arcs|    14.6 ms|     8.4 ms |
| `insurance.bif`                |   27 nodes|  52 arcs|     5.5 ms|     6.9 ms |
| `simpleCovid19.bif`            |    8 nodes|   7 arcs|     0.9 ms|     0.5 ms |

## bifxml

|    file                        |      structure    | |    load   |  inference |
|:------------------------------:|----------:|--------:|----------:|-----------:|
| `Diabetes.bifxml`              |  413 nodes| 602 arcs|   195.5 ms|   222.9 ms |
| `Link.bifxml`                  |  724 nodes|1125 arcs|    26.8 ms|87,330.1 ms |
| `Mildew.bifxml`                |   35 nodes|  46 arcs|   169.5 ms|   215.4 ms |
| `Munin1.bifxml`                |  186 nodes| 273 arcs|    14.0 ms|18,048.5 ms |
| `Pigs.bifxml`                  |  441 nodes| 592 arcs|    14.1 ms|   117.3 ms |
| `Water.bifxml`                 |   32 nodes|  66 arcs|     7.1 ms|   290.3 ms |
| `alarm.bifxml`                 |   37 nodes|  46 arcs|     1.4 ms|     4.7 ms |
| `carpo.bifxml`                 |   61 nodes|  74 arcs|     1.7 ms|     6.9 ms |
| `dog.bifxml`                   |    5 nodes|   4 arcs|     0.2 ms|     0.3 ms |
| `hailfinder.bifxml`            |   56 nodes|  66 arcs|     3.4 ms|     8.4 ms |
| `insurance.bifxml`             |   27 nodes|  52 arcs|     1.6 ms|     6.9 ms |

## dsl

|    file                        |      structure     | |   load   |  inference |
|:------------------------------:|----------:|---------:|---------:|-----------:|
| `Barley.dsl`                   |   47 nodes|  82 arcs|   260.4 ms| 1,181.7 ms |
| `Diabetes.dsl`                 |  413 nodes| 602 arcs|   672.8 ms|   226.3 ms |
| `Ling.dsl`                     |   13 nodes|  17 arcs|     3.2 ms|     1.1 ms |
| `Link.dsl`                     |  724 nodes|1125 arcs|    43.0 ms|104,615.8 ms |
| `Mildew.dsl`                   |   35 nodes|  46 arcs|   710.5 ms|   237.6 ms |
| `Munin1.dsl`                   |  186 nodes| 273 arcs|    33.5 ms|10,711.1 ms |
| `Pigs.dsl`                     |  441 nodes| 592 arcs|    22.2 ms|   136.3 ms |
| `Water.dsl`                    |   32 nodes|  66 arcs|    22.1 ms|   286.2 ms |
| `alarm.dsl`                    |   37 nodes|  46 arcs|     2.2 ms|     4.5 ms |
| `carpo.dsl`                    |   61 nodes|  74 arcs|     2.2 ms|     7.1 ms |
| `hailfinder.dsl`               |   56 nodes|  66 arcs|     7.6 ms|     8.4 ms |
| `insurance.dsl`                |   27 nodes|  52 arcs|     3.2 ms|     6.9 ms |

## net

|    file                        |      structure     | |   load   |  inference |
|:------------------------------:|----------:|---------:|---------:|-----------:|
| `main4.net`                    |  910 nodes|1250 arcs| 4,789.2 ms| - |
| `ship-ship.net`                |   50 nodes|  75 arcs|   179.0 ms|45,445.0 ms |

## o3prm

|    file                        |      structure     | |   load    | inference |
|:------------------------------:|----------:|---------:|----------:|----------:|
| `Asia.o3prm`                   |    8 nodes|   8 arcs|     3.2 ms|     0.7 ms |
| `complexprinters_system.o3prm` |  164 nodes| 215 arcs|    23.5 ms| 7,370.2 ms |

## uai

|    file                        |      structure     | |   load    | inference |
|:------------------------------:|----------:|---------:|----------:|----------:|
| `asia.uai`                     |    8 nodes|   8 arcs|     0.3 ms|     0.7 ms |
| `dog.uai`                      |    5 nodes|   4 arcs|     0.2 ms|     0.3 ms |

