
# PGMrepository

A repository for as many PGMs files and formats as possible ,with some benchmarks.

# Content

Benchmarks for (almost) all the models in this repository using aGrUM's multithreaded(10) Lazy Propagation.
Time average on 20 runs.

## bif

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Mildew.bif`                   |   35 nodes|  46 arcs|   5|   144.4 ms|    46.8 ms |
| `Barley.bif`                   |   48 nodes|  84 arcs|   8|   164.3 ms|   109.8 ms |
| `Diabetes.bif`                 |  413 nodes| 602 arcs|   6|   325.2 ms|   108.8 ms |
| `Munin1.bif`                   |  186 nodes| 273 arcs|  12|    29.0 ms| 1,482.1 ms |
| `Pigs.bif`                     |  441 nodes| 592 arcs|  11|    37.3 ms|   110.0 ms |
| `Water.bif`                    |   32 nodes|  66 arcs|  12|    17.4 ms|    50.2 ms |
| `alarm.bif`                    |   37 nodes|  46 arcs|   5|     1.4 ms|     5.0 ms |
| `asia.bif`                     |    8 nodes|   8 arcs|   3|     0.1 ms|     0.5 ms |
| `carpo.bif`                    |   61 nodes|  74 arcs|   6|     6.7 ms|     8.5 ms |
| `child.bif`                    |   20 nodes|  25 arcs|   4|     0.6 ms|     2.4 ms |
| `complete_carpo.bif`           |   61 nodes|  74 arcs|   6|     1.2 ms|     8.5 ms |
| `covid19.bif`                  |   39 nodes|  46 arcs|   5|     1.3 ms|     4.5 ms |
| `hailfinder.bif`               |   56 nodes|  66 arcs|   5|     5.4 ms|     8.6 ms |
| `insurance.bif`                |   27 nodes|  52 arcs|   8|     2.0 ms|     7.3 ms |
| `simpleCovid19.bif`            |    8 nodes|   7 arcs|   4|     0.3 ms|     0.4 ms |

## bifxml

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Diabetes.bifxml`              |  413 nodes| 602 arcs|   6|   310.6 ms|   110.0 ms |
| `Link.bif`                     |  724 nodes|1125 arcs|  20|    77.3 ms|10,281.0 ms |
| `Mildew.bif`                   |   35 nodes|  46 arcs|   5|   155.7 ms|    50.8 ms |
| `Munin1.bif`                   |  186 nodes| 273 arcs|  12|    29.8 ms| 1,499.8 ms |
| `Link.bifxml`                  |  724 nodes|1125 arcs|  20|    20.3 ms|10,641.4 ms |
| `Pigs.bif`                     |  441 nodes| 592 arcs|  11|    32.5 ms|   108.9 ms |
| `Water.bif`                    |   32 nodes|  66 arcs|  12|    17.3 ms|    50.1 ms |
| `alarm.bif`                    |   37 nodes|  46 arcs|   5|     1.3 ms|     4.8 ms |
| `asia.bif`                     |    8 nodes|   8 arcs|   3|     0.1 ms|     0.5 ms |
| `carpo.bif`                    |   61 nodes|  74 arcs|   6|     6.8 ms|     8.2 ms |
| `child.bif`                    |   20 nodes|  25 arcs|   4|     0.6 ms|     2.3 ms |
| `complete_carpo.bif`           |   61 nodes|  74 arcs|   6|     1.1 ms|     8.4 ms |
| `covid19.bif`                  |   39 nodes|  46 arcs|   5|     1.3 ms|     4.3 ms |
| `hailfinder.bif`               |   56 nodes|  66 arcs|   5|     5.3 ms|     8.3 ms |
| `insurance.bif`                |   27 nodes|  52 arcs|   8|     1.9 ms|     7.3 ms |
| `simpleCovid19.bif`            |    8 nodes|   7 arcs|   4|     0.3 ms|     0.4 ms |

## bifxml

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Mildew.bifxml`                |   35 nodes|  46 arcs|   5|   358.6 ms|    45.4 ms |
| `Diabetes.bifxml`              |  413 nodes| 602 arcs|   6|   311.4 ms|   104.3 ms |
| `Munin1.bifxml`                |  186 nodes| 273 arcs|  12|    15.3 ms| 1,580.4 ms |
| `Pigs.bifxml`                  |  441 nodes| 592 arcs|  11|    10.8 ms|   115.3 ms |
| `Water.bifxml`                 |   32 nodes|  66 arcs|  12|     9.5 ms|    48.4 ms |
| `alarm.bifxml`                 |   37 nodes|  46 arcs|   5|     0.8 ms|     5.1 ms |
| `carpo.bifxml`                 |   61 nodes|  74 arcs|   6|     0.8 ms|     8.5 ms |
| `dog.bifxml`                   |    5 nodes|   4 arcs|   3|     0.1 ms|     0.3 ms |
| `hailfinder.bifxml`            |   56 nodes|  66 arcs|   5|     3.0 ms|     8.5 ms |
| `insurance.bifxml`             |   27 nodes|  52 arcs|   8|     1.2 ms|     7.8 ms |

## dsl

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Barley.dsl`                   |   47 nodes|  82 arcs|   8|   125.8 ms|   107.0 ms |
| `Diabetes.dsl`                 |  413 nodes| 602 arcs|   6|   163.7 ms|   105.4 ms |
| `Ling.dsl`                     |   13 nodes|  17 arcs|   5|     1.3 ms|     1.0 ms |
| `Link.bifxml`                  |  724 nodes|1125 arcs|  20|    19.6 ms| 9,802.3 ms |
| `Mildew.bifxml`                |   35 nodes|  46 arcs|   5|   354.4 ms|    50.9 ms |
| `Munin1.bifxml`                |  186 nodes| 273 arcs|  12|    14.9 ms| 1,623.9 ms |
| `Pigs.bifxml`                  |  441 nodes| 592 arcs|  11|     9.0 ms|   109.4 ms |
| `Water.bifxml`                 |   32 nodes|  66 arcs|  12|     9.8 ms|    49.3 ms |
| `alarm.bifxml`                 |   37 nodes|  46 arcs|   5|     0.8 ms|     4.9 ms |
| `carpo.bifxml`                 |   61 nodes|  74 arcs|   6|     0.8 ms|     8.3 ms |
| `dog.bifxml`                   |    5 nodes|   4 arcs|   3|     0.1 ms|     0.3 ms |
| `hailfinder.bifxml`            |   56 nodes|  66 arcs|   5|     3.1 ms|     8.7 ms |
| `insurance.bifxml`             |   27 nodes|  52 arcs|   8|     1.2 ms|     7.0 ms |

## dsl

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Barley.dsl`                   |   47 nodes|  82 arcs|   8|   124.5 ms|   113.2 ms |
| `Diabetes.dsl`                 |  413 nodes| 602 arcs|   6|   166.0 ms|   112.0 ms |
| `Ling.dsl`                     |   13 nodes|  17 arcs|   5|     1.4 ms|     1.0 ms |
| `Link.dsl`                     |  724 nodes|1125 arcs|  20|     8.9 ms|14,095.5 ms |
| `Mildew.dsl`                   |   35 nodes|  46 arcs|   5|   130.4 ms|    48.7 ms |
| `Munin1.dsl`                   |  186 nodes| 273 arcs|  12|    10.3 ms| 1,005.7 ms |
| `Pigs.dsl`                     |  441 nodes| 592 arcs|  12|     7.5 ms|   113.7 ms |
| `Water.dsl`                    |   32 nodes|  66 arcs|  12|     7.0 ms|    48.0 ms |
| `alarm.dsl`                    |   37 nodes|  46 arcs|   5|     1.0 ms|     4.7 ms |
| `carpo.dsl`                    |   61 nodes|  74 arcs|   6|     0.7 ms|     7.9 ms |
| `hailfinder.dsl`               |   56 nodes|  66 arcs|   5|     3.2 ms|     8.1 ms |
| `insurance.dsl`                |   27 nodes|  52 arcs|   8|     1.2 ms|     7.2 ms |

## net

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `main4.net`                    |  910 nodes|1250 arcs|  32| 1,072.8 ms|- |
| `ship-ship.net`                |   50 nodes|  75 arcs|  10|    31.7 ms| 4,636.5 ms |

## o3prm

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Asia.o3prm`                   |    8 nodes|   8 arcs|   3|     1.3 ms|     0.5 ms |
| `complexprinters_system.o3prm` |  164 nodes| 215 arcs|  15|    12.7 ms| 1,384.7 ms |

## uai

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `asia.uai`                     |    8 nodes|   8 arcs|   3|     0.1 ms|     0.5 ms |
| `dog.uai`                      |    5 nodes|   4 arcs|   3|     0.0 ms|     0.2 ms |

| `Link.dsl`                     |  724 nodes|1125 arcs|  20|     9.1 ms|13,622.4 ms |
| `Mildew.dsl`                   |   35 nodes|  46 arcs|   5|   124.9 ms|    46.3 ms |
| `Munin1.dsl`                   |  186 nodes| 273 arcs|  12|    10.3 ms|   939.5 ms |
| `Pigs.dsl`                     |  441 nodes| 592 arcs|  12|     6.9 ms|   109.7 ms |
| `Water.dsl`                    |   32 nodes|  66 arcs|  12|     7.1 ms|    48.3 ms |
| `alarm.dsl`                    |   37 nodes|  46 arcs|   5|     0.9 ms|     4.8 ms |
| `carpo.dsl`                    |   61 nodes|  74 arcs|   6|     0.6 ms|     8.0 ms |
| `hailfinder.dsl`               |   56 nodes|  66 arcs|   5|     3.0 ms|     8.1 ms |
| `insurance.dsl`                |   27 nodes|  52 arcs|   8|     1.1 ms|     6.6 ms |

## net

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `main4.net`                    |  910 nodes|1250 arcs|  32| 1,027.1 ms|- |
| `ship-ship.net`                |   50 nodes|  75 arcs|  10|    28.7 ms| 4,505.9 ms |

## o3prm

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Asia.o3prm`                   |    8 nodes|   8 arcs|   3|     1.1 ms|     0.5 ms |
| `complexprinters_system.o3prm` |  164 nodes| 215 arcs|  15|    12.7 ms| 1,440.8 ms |

## uai

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `asia.uai`                     |    8 nodes|   8 arcs|   3|     0.1 ms|     0.5 ms |
| `dog.uai`                      |    5 nodes|   4 arcs|   3|     0.0 ms|     0.3 ms |

