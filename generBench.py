"""
A module to generate benchmarks on a set of PGM models

(C) PHW 2022
"""
import sys
import os
from pathlib import Path
from timeit import default_timer as timer

import pyAgrum as gum

#from pgmpy.models import BayesianNetwork as pgmpyBN
#from pgmpy.inference import BeliefPropagation as pgmpuInference


class BNBenchmark:
  """
  Virtual base class for benchmarking
  """

  def __init__(self):
    self.msg = "(not implemented)"

  def tryToRead(self, dirname: str, filename: str):
    """
    Load a model

    :param dirname: the folder
    :param filename:  the filename
    :return: the model and the time to read it as a couple
    """
    raise NotImplementedError

  def tryToInfer(self, bn):
    """
    Compute a full propagation (all the marginal posterior with no evidence)
    :param bn: the mpdel
    :return: the time
    """
    raise NotImplementedError


# unfinished test
#class PgmpyBNBenchmark(BNBenchmark):
#  """
#  Concrete Base class for benchmarking with pyAgrum
#  """
#
#  def __init__(self):
#    super().__init__()
#    self.msg = "pgmpy's belief propagation"
#
#  def tryToRead(self, dirname, filename):
#    start1 = timer()
#    bn = pgmpyBN.load(f"{dirname}/{filename}")
#    end1 = timer()
#
#    return bn, end1 - start1
#
#  def tryToInfer(self, bn):
#    start2 = timer()
#    marg = []
#    bp = pgmpuInference(bn)
#    for n in bn.nodes:
#        marg.append(bp.query(variables=[n], evidence={}, show_progress=False))
#    end2 = timer()
#
#    return end2 - start2


class GumBNBenchmark(BNBenchmark):
  """
  Concrete Base class for benchmarking with pyAgrum
  """

  def __init__(self):
    super().__init__()
    self.msg = "aGrUM's multithreaded(10) Lazy Propagation"
    gum.setNumberOfThreads(10)

  def tryToRead(self, dirname, filename):
    start1 = timer()
    bn = gum.loadBN(f"{dirname}/{filename}")
    end1 = timer()

    return bn, end1 - start1

  def tryToInfer(self, bn):
    start2 = timer()
    ie = gum.LazyPropagation(bn)
    ie.setNumberOfThreads(0)
    ie.makeInference()
    end2 = timer()

    return end2 - start2

class MonoThreadBNBenchmark(GumBNBenchmark):
  """
  Concrete class for benchmarking with pyAgrum and mono-thread inference
  """

  def __init__(self):
    super().__init__()
    self.msg = "aGrUM's monothread Lazy Propagation"
    gum.setNumberOfThreads(1)
  

def treeWidth(bn):
  ie=gum.LazyPropagation(bn)
  jt=ie.junctionTree()
  return max([len(jt.clique(c)) for c in jt.nodes()])

def generateLoadAndInferForAModel(benchmark: BNBenchmark,
                                  dirname: str, filename: str, nbiter: int,
                                  tryToInfer: bool = True, mdfile: str = None):
  """
  Generate a report for load and basic inference for a model

  :param benchmark: the benchmark
  :param dirname: the folder
  :param filename: the model
  :param nbiter: the nb of iterations
  :param tryToInfer: a way to not infer from special (too large) model
  :param mdfile: the markdown file
  """
  sum_read = 0
  nb_read = 0
  sum_inference = 0
  nb_inference = 0
  nb_nodes = "?"
  nb_arcs = "?"
  treew = "?"

  print(f"  - {filename:30} [", flush=True, end='')
  md = f'| {("`" + filename + "`"):30}'
  for _ in range(nbiter):
    print(".", flush=True, end='')
    try:
      bn, read_duration = benchmark.tryToRead(dirname, filename)
    except:
      bn = None
      read_duration = -1
      inference_duration = -1

    sum_read += read_duration
    nb_read += 1
    nb_nodes = f"{bn.size():4d} nodes"
    nb_arcs = f"{bn.sizeArcs():4d} arcs"
    treew = f"{treeWidth(bn):4d}"

    if bn is not None and tryToInfer:
      try:
        inference_duration = benchmark.tryToInfer(bn)
      except:
        inference_duration = -1
        continue

      sum_inference += inference_duration
      nb_inference += 1

  if nb_read == 0:
    average_read = "-"
  else:
    average_read = f"{1000 * sum_read / nb_read:8,.1f} ms"
  if nb_inference == 0:
    average_inference = "-"
  else:
    average_inference = f"{1000 * sum_inference / nb_inference:8,.1f} ms"

  print(f"] : {nb_nodes} {nb_arcs} {treew} {average_read} {average_inference}")
  md += f" | {nb_nodes}|{nb_arcs}|{treew}|{average_read}|{average_inference} |"
  if mdfile is not None:
    with open(mdfile, "a") as mdf:
      print(md, file=mdf)


def generateLoadAndModel(benchmark: BNBenchmark,
                         nbiter: int,
                         files_not_used=None, models_not_inferred=None,
                         mdfile: str = None):
  """
  Generate a report for load and basic inference for all model in the current path

  :param benchmark: the benchmark
  :param nbiter: the nb of iterations
  :param files_not_used: files excluded
  :param model_not_inferred: models loaded but not infered (too large for instance)
  :param mdfile: the markdown file
  """
  print(benchmark.msg)
  if mdfile is not None:
    print(f"** Writing results in {mdfile}")
    with open(mdfile, "w") as mdf:
      print(f"""
# PGMrepository

A repository for as many PGMs files and formats as possible ,with some benchmarks.

# Content

Benchmarks for (almost) all the models in this repository using {benchmark.msg}.
Time average on {nbiter} runs.
""", file=mdf)

  print(f"{' file ':*^{30 + 7 + nbiter}}  {' structure ':*^21} {' load ':*^12} {' LazyP ':*^13}")

  p = Path(".")
  for dirName, _, filenames in sorted(os.walk(p)):
    if dirName == ".":
      continue
    if dirName.count("/")>1: # no subfolder for now ...
      continue
    if not dirName.startswith(('./.', '.\\.','./_')):
      print(f"+ {dirName}")

      if mdfile is not None:
        with open(mdfile, "a") as mdf:
          print(f"## {dirName[2:]}", file=mdf)
          print("", file=mdf)

          print("|    file                        |      structure      || Treewidth |   load    | inference |", file=mdf)
          print("|:------------------------------:|----------:|---------:|---------:|----------:|----------:|", file=mdf)

      for filename in sorted(filenames):
        if filename not in files_not_used:
          generateLoadAndInferForAModel(benchmark,
                                        dirName, filename, nbiter,
                                        tryToInfer=filename not in models_not_inferred,
                                        mdfile=mdfile)

      if mdfile is not None:
        with open(mdfile, "a") as mdf:
          print("", file=mdf)

  print(f"** {mdfile} written.")


nb_iteration =20
no_generation_for = {"README.md"}
no_inference_for = {"main4.net"}  # too many memory is needed

if len(sys.argv) == 1:
  generateLoadAndModel(MonoThreadBNBenchmark(), nb_iteration, no_generation_for, no_inference_for, "Readme-mono.md")
  generateLoadAndModel(GumBNBenchmark(), nb_iteration, no_generation_for, no_inference_for, "Readme-multi10.md")
else:
  print(f"{' file ':*^{30 + 7 + nb_iteration}}  {' structure ':*^21} {' load ':*^12} {' LazyP ':*^13}")
  request = sys.argv[1]
  generateLoadAndInferForAModel(MonoThreadBNBenchmark(), dirname=".", filename=request, nbiter=nb_iteration,
                                tryToInfer=request not in no_inference_for, mdfile="Readle.md")
