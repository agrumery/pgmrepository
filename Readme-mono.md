
# PGMrepository

A repository for as many PGMs files and formats as possible ,with some benchmarks.

# Content

Benchmarks for (almost) all the models in this repository using aGrUM's monothread Lazy Propagation.
Time average on 20 runs.

## bif

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Barley.bif`                   |   48 nodes|  84 arcs|   8|   163.1 ms|   226.3 ms |
| `Diabetes.bif`                 |  413 nodes| 602 arcs|   6|   311.4 ms|   139.9 ms |
| `Link.bif`                     |  724 nodes|1125 arcs|  20|    72.0 ms|23,358.5 ms |
| `Mildew.bif`                   |   35 nodes|  46 arcs|   5|   146.1 ms|    93.8 ms |
| `Munin1.bif`                   |  186 nodes| 273 arcs|  12|    28.6 ms| 3,780.6 ms |
| `Pigs.bif`                     |  441 nodes| 592 arcs|  11|    30.6 ms|    98.4 ms |
| `Water.bif`                    |   32 nodes|  66 arcs|  12|    16.7 ms|   102.2 ms |
| `alarm.bif`                    |   37 nodes|  46 arcs|   5|     1.3 ms|     5.0 ms |
| `asia.bif`                     |    8 nodes|   8 arcs|   3|     0.1 ms|     0.5 ms |
| `carpo.bif`                    |   61 nodes|  74 arcs|   6|     6.4 ms|     8.3 ms |
| `child.bif`                    |   20 nodes|  25 arcs|   4|     0.6 ms|     2.3 ms |
| `complete_carpo.bif`           |   61 nodes|  74 arcs|   6|     1.1 ms|     8.3 ms |
| `covid19.bif`                  |   39 nodes|  46 arcs|   5|     1.4 ms|     4.4 ms |
| `hailfinder.bif`               |   56 nodes|  66 arcs|   5|     5.6 ms|     8.7 ms |
| `insurance.bif`                |   27 nodes|  52 arcs|   8|     2.0 ms|     7.8 ms |
| `simpleCovid19.bif`            |    8 nodes|   7 arcs|   4|     0.3 ms|     0.4 ms |

## bifxml

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Diabetes.bifxml`              |  413 nodes| 602 arcs|   6|   315.7 ms|   167.2 ms |
| `Link.bif`                     |  724 nodes|1125 arcs|  20|    73.5 ms|23,812.7 ms |
| `Mildew.bif`                   |   35 nodes|  46 arcs|   5|   147.2 ms|    93.3 ms |
| `Munin1.bif`                   |  186 nodes| 273 arcs|  12|    31.4 ms| 3,865.7 ms |
| `Pigs.bif`                     |  441 nodes| 592 arcs|  11|    30.7 ms|   102.0 ms |
| `Water.bif`                    |   32 nodes|  66 arcs|  12|    17.2 ms|   105.1 ms |
| `alarm.bif`                    |   37 nodes|  46 arcs|   5|     1.5 ms|     5.0 ms |
| `asia.bif`                     |    8 nodes|   8 arcs|   3|     0.1 ms|     0.5 ms |
| `carpo.bif`                    |   61 nodes|  74 arcs|   6|     7.1 ms|     8.6 ms |
| `child.bif`                    |   20 nodes|  25 arcs|   4|     0.8 ms|     2.4 ms |
| `complete_carpo.bif`           |   61 nodes|  74 arcs|   6|     1.2 ms|     8.6 ms |
| `covid19.bif`                  |   39 nodes|  46 arcs|   5|     1.5 ms|     4.5 ms |
| `hailfinder.bif`               |   56 nodes|  66 arcs|   5|     5.6 ms|     8.6 ms |
| `insurance.bif`                |   27 nodes|  52 arcs|   8|     1.9 ms|     7.7 ms |
| `simpleCovid19.bif`            |    8 nodes|   7 arcs|   4|     0.3 ms|     0.4 ms |

## bifxml

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Diabetes.bifxml`              |  413 nodes| 602 arcs|   6|   309.6 ms|   154.7 ms |
| `Link.bifxml`                  |  724 nodes|1125 arcs|  20|    19.4 ms|24,269.7 ms |
| `Mildew.bifxml`                |   35 nodes|  46 arcs|   5|   355.7 ms|    81.3 ms |
| `Munin1.bifxml`                |  186 nodes| 273 arcs|  12|    14.8 ms| 3,870.7 ms |
| `Pigs.bifxml`                  |  441 nodes| 592 arcs|  11|     9.2 ms|   107.9 ms |
| `Water.bifxml`                 |   32 nodes|  66 arcs|  12|     9.4 ms|   101.0 ms |
| `alarm.bifxml`                 |   37 nodes|  46 arcs|   5|     0.7 ms|     4.9 ms |
| `carpo.bifxml`                 |   61 nodes|  74 arcs|   6|     0.7 ms|     8.1 ms |
| `dog.bifxml`                   |    5 nodes|   4 arcs|   3|     0.1 ms|     0.3 ms |
| `hailfinder.bifxml`            |   56 nodes|  66 arcs|   5|     2.8 ms|     8.3 ms |
| `insurance.bifxml`             |   27 nodes|  52 arcs|   8|     1.1 ms|     7.2 ms |

## dsl

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Barley.dsl`                   |   47 nodes|  82 arcs|   8|   120.2 ms|   259.7 ms |
| `Diabetes.dsl`                 |  413 nodes| 602 arcs|   6|   161.0 ms|   150.3 ms |
| `Ling.dsl`                     |   13 nodes|  17 arcs|   5|     1.4 ms|     1.0 ms |
| `Link.bifxml`                  |  724 nodes|1125 arcs|  20|    20.0 ms|24,791.3 ms |
| `Mildew.bifxml`                |   35 nodes|  46 arcs|   5|   359.4 ms|    89.5 ms |
| `Munin1.bifxml`                |  186 nodes| 273 arcs|  12|    14.5 ms| 3,729.9 ms |
| `Pigs.bifxml`                  |  441 nodes| 592 arcs|  11|     9.1 ms|   108.1 ms |
| `Water.bifxml`                 |   32 nodes|  66 arcs|  12|     9.6 ms|   100.6 ms |
| `alarm.bifxml`                 |   37 nodes|  46 arcs|   5|     0.8 ms|     5.0 ms |
| `carpo.bifxml`                 |   61 nodes|  74 arcs|   6|     0.8 ms|     8.3 ms |
| `dog.bifxml`                   |    5 nodes|   4 arcs|   3|     0.1 ms|     0.3 ms |
| `hailfinder.bifxml`            |   56 nodes|  66 arcs|   5|     3.0 ms|     8.8 ms |
| `insurance.bifxml`             |   27 nodes|  52 arcs|   8|     1.2 ms|     7.3 ms |

## dsl

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Barley.dsl`                   |   47 nodes|  82 arcs|   8|   121.6 ms|   273.3 ms |
| `Diabetes.dsl`                 |  413 nodes| 602 arcs|   6|   164.7 ms|   163.5 ms |
| `Ling.dsl`                     |   13 nodes|  17 arcs|   5|     1.4 ms|     1.0 ms |
| `Link.dsl`                     |  724 nodes|1125 arcs|  20|     8.7 ms|32,570.3 ms |
| `Mildew.dsl`                   |   35 nodes|  46 arcs|   5|   126.3 ms|    91.7 ms |
| `Munin1.dsl`                   |  186 nodes| 273 arcs|  12|     9.8 ms| 2,241.0 ms |
| `Pigs.dsl`                     |  441 nodes| 592 arcs|  12|     6.3 ms|   124.5 ms |
| `Water.dsl`                    |   32 nodes|  66 arcs|  12|     7.0 ms|    95.7 ms |
| `alarm.dsl`                    |   37 nodes|  46 arcs|   5|     0.8 ms|     4.6 ms |
| `carpo.dsl`                    |   61 nodes|  74 arcs|   6|     0.8 ms|     7.8 ms |
| `hailfinder.dsl`               |   56 nodes|  66 arcs|   5|     3.2 ms|     7.9 ms |
| `insurance.dsl`                |   27 nodes|  52 arcs|   8|     1.1 ms|     6.9 ms |

## net

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `main4.net`                    |  910 nodes|1250 arcs|  32| 1,035.7 ms|- |
| `Link.dsl`                     |  724 nodes|1125 arcs|  20|     8.7 ms|31,499.2 ms |
| `Mildew.dsl`                   |   35 nodes|  46 arcs|   5|   122.0 ms|    85.4 ms |
| `Munin1.dsl`                   |  186 nodes| 273 arcs|  12|     9.8 ms| 2,410.9 ms |
| `Pigs.dsl`                     |  441 nodes| 592 arcs|  12|     6.1 ms|   120.6 ms |
| `Water.dsl`                    |   32 nodes|  66 arcs|  12|     7.0 ms|    98.6 ms |
| `alarm.dsl`                    |   37 nodes|  46 arcs|   5|     0.9 ms|     4.8 ms |
| `ship-ship.net`                |   50 nodes|  75 arcs|  10|    29.4 ms|10,488.3 ms |

## o3prm

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `carpo.dsl`                    |   61 nodes|  74 arcs|   6|     1.3 ms|     8.1 ms |
| `Asia.o3prm`                   |    8 nodes|   8 arcs|   3|     1.2 ms|     0.5 ms |
| `hailfinder.dsl`               |   56 nodes|  66 arcs|   5|     3.4 ms|     8.5 ms |
| `insurance.dsl`                |   27 nodes|  52 arcs|   8|     1.2 ms|     7.4 ms |

## net

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `main4.net`                    |  910 nodes|1250 arcs|  32| 1,037.3 ms|- |
| `complexprinters_system.o3prm` |  164 nodes| 215 arcs|  15|    11.9 ms| 3,134.2 ms |

## uai

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `asia.uai`                     |    8 nodes|   8 arcs|   3|     0.1 ms|     0.5 ms |
| `dog.uai`                      |    5 nodes|   4 arcs|   3|     0.0 ms|     0.2 ms |

| `ship-ship.net`                |   50 nodes|  75 arcs|  10|    29.7 ms|10,432.5 ms |

## o3prm

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `Asia.o3prm`                   |    8 nodes|   8 arcs|   3|     1.3 ms|     0.6 ms |
| `complexprinters_system.o3prm` |  164 nodes| 215 arcs|  15|    13.5 ms| 3,459.3 ms |

## uai

|    file                        |      structure      || Treewidth |   load    | inference |
|:------------------------------:|----------:|---------:|---------:|----------:|----------:|
| `asia.uai`                     |    8 nodes|   8 arcs|   3|     0.1 ms|     0.5 ms |
| `dog.uai`                      |    5 nodes|   4 arcs|   3|     0.0 ms|     0.3 ms |

