## PGMs description

- simpleCovid19.bif was described in https://www.researchgate.net/publication/343080049_A_privacy-preserving_Bayesian_network_model_for_personalised_COVID19_risk_assessment_and_contact_tracing/fulltext/5f15b5224585151299aaeaad/A-privacy-preserving-Bayesian-network-model-for-personalised-COVID19-risk-assessment-and-contact-tracing.pdf?origin=publication_detail
- covid19.bif has been generated from http://www.eecs.qmul.ac.uk/~norman/Models/covid19_for_contact_tracing_paper.cmpx